import axios, {AxiosInstance} from "axios";

export class Travis {

    private api: AxiosInstance

    constructor(token: string) {
        this.api = axios.create({
            baseURL: "https://api.travis-ci.com/v3",
            headers: {
                "Travis-API-Version": "3",
                "Authorization": "token " + token
            }
        })
    }

    async getOrgs(): Promise<number[]> {
        const {data} = await this.api.get("/orgs")
        return data['organizations'].map((el: { [x: string]: any; }) => el['id'])
    }

    async getRepos(): Promise<number[]> {
        const {data} = await this.api.get("/repos?active=true")
        return data['repositories'].map((el: { [x: string]: any; }) => el['id'])
    }

    async runBuilds() {
        const repos = await this.getRepos()
        for (const repo of repos) {
            const builds = await this.api.get('/repo/' + repo + '/builds')
            const ids = builds.data['builds']
                .filter((el: { [x: string]: string; }) => el['state'] !== "created" && el['state'] !== "started")
                .filter((el: { [x: string]: any }) => new Date(el['finished_at']).getTime() - new Date(el['started_at']).getTime() > 120000)
                .map((el: { [x: string]: any; }) => el['id']) as number[]
            await Promise.all(ids.slice(0, Math.min(15, ids.length)).map(el => this.api.post(`/build/${el}/restart`)))
            console.log("Build Restart of", repo, ":", Math.min(15, ids.length))
        }
    }

    async cancelBuilds() {
        const repos = await this.getRepos()
        for (const repo of repos) {
            const builds = await this.api.get('/repo/' + repo + '/builds')
            const ids = builds.data['builds']
                .filter((el: { [x: string]: string; }) => el['state'] === "created" || el['state'] === "started")
                .map((el: { [x: string]: any; }) => el['id']) as number[]
            await Promise.all(ids.map(el => this.api.post(`/build/${el}/cancel`)))
            console.log("Build Cancelled of", repo, ":", ids.length)
        }
    }

    async consumeOSSCreditsEnable() {
        await this.api.patch(`/preference/consume_oss_credits`, {'preference.value': true})
        const orgs = await this.getOrgs()
        await Promise.all(orgs.map(el => this.api.patch(`/org/${el}/preference/consume_oss_credits`, {'preference.value': true})))
        console.log("Consume Credits Enabled")
    }

    async consumeOSSCreditsDisable() {
        await this.api.patch(`/preference/consume_oss_credits`, {'preference.value': false})
        const orgs = await this.getOrgs()
        await Promise.all(orgs.map((el: any) => this.api.patch(`/org/${el}/preference/consume_oss_credits`, {'preference.value': false})))
        console.log("Consume Credits Disabled")
    }

}